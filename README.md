# Тестовое задание для поступления в DevOps Cloud.ru Camp
Результаты выполнения тестового задания следует опубликовать на GitHub или разместить на любой открытой платформе (например, Github Pages) и отправить на почту devopscloudcamp@cloud.ru. Также следует указать свои контактные данные для получения обратной связи.

## 1. Ansible playbook
Для запуска playbook необходимо:
1 Внести изменения в файл hosts:
*  заменить значение ansible_host на необходимое Вам
*  заменить значение ansible_user на необходимое Вам
*  заменить значение ansible_password на необходимое Вам
2 Запустить useradd.yml в каталоге your_repo/playbook.
```bash
ansible-playbook useradd.yml --ask-become-pass
```
## 2. Веб-приложение на Python
* При реализации api был создан скрипт на `Python` с использованием фреймворк `Fastapi`.
* Созданы точки: 
`/id` 
`/hostname`
`/author` 
`/readiness` 
`/liveness`
* Создан файл зависимостей для запуска приложений `requirements.txt`.

### 2.1. Как настроить и проверить приложение
1. Настроить виртуальное окружение
```bash
python3.9 -m venv venv
source venv/bin/activate
```
2. Настроить зависимости
```bash
pip install -r requirements.txt
```
3. Запустить приложение
```bash
python main.py
```
4. Деактивировать виртуальное окружение
```bash
deactivate
```
### 2.2. Dockerfile и docker-compose
Dockerfile был создан в каталоге your_repo/app.
Также были созданы `docker-compose.yml` и конфигурационный файл для `nginx`.
Создание этих файлов позволяет развернуть три реплики приложения на `Python`.
Тестирование настроено в `docker-compose.yml`.
1. Собрать образ docker
```bash
docker build -t your_hub_name/image_name:tag .
# If necessary, push to your custom Docker registry or Docker Hub
docker push your_hub_name/image_name:tag
```
2. Запустить приложение из образа docker
```bash
docker run -p 8000:8000 --hostname='your_name' your_hub_name/image_name:tag -d
```
3. Запустить приложение c помощью docker compose
```bash
docker compose up -d --build
```
### 2.3. Kubernetes
* Для развертывания приложения создан манифест `uvicorn.yml`.
* Пространство имен uvicorn задается в манифесте `namespace.yml`.
* Для обслуживания развертывания был создан манифест с типом ClusterIP `service.yml`.

1. Как развернуть приложение
```bash
cd ~/your_repo/manifest
kubectl apply -f ./
```
2. Как проверить приложение
Проверьте готовность ваших капсул и вашего сервиса
```bash
kubectl get pod -n your_namespace
kubectl get svc -n your_namespace
# or inspect events to catch errors
watch kubectl events -n your_namespace
# or inspect pod logs
kubectl logs pod_name -n your_namespace --watch
```
Проверьте работу вашего приложения
```bash
curl http://localhost:8000/docs
```
### 2.4. Helm
Для развертывания helm необходимо:
1. Перейти в папку helm
```bash
cd ~/your_repo/helm
```
2. Убедиться, что ваши манифесты созданы правильно
```bash
helm template .
```
3. Развернуть приложение
```bash
helm install your_release_name . -n your_namespace
```

