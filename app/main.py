#pip install fastapi
#pip install "uvicorn[standard]"
#pip freeze > requirements.txt
#pip install -r requirements.txt
#python main.py
import uvicorn
import socket
from fastapi import FastAPI
from uuid import uuid4
import urllib.request

AUTHOR = 'Aleksey Zverev'
app = FastAPI()
UUID = uuid4()
name = socket.gethostname()
myip = socket.gethostbyname(name)
interfaces = socket.if_nameindex()
external_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')


@app.get("/author")
async def author():
    return {'AUTHOR': AUTHOR}


@app.get("/id")
async def root():
    return {'uuid': UUID}


@app.get("/hostname")
async def hostname():
    return {
        "Name of the host is {}".format(name),
        "IP of the host is {}".format(myip),
        "External IP of the host {}".format(external_ip),
        "Interfaces of the host {}".format(interfaces),
    }
    
@app.get("/readiness")
def readiness():
    is_ready()
    return {'status': "Ready"}


@app.get("/liveness")
def liveness():
    return {'status': "Alive"}
    

if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, reload=True)
